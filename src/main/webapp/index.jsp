<%-- 
    Document   : index
    Created on : 04-05-2021, 22:43:09
    Author     : Karukami
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OxfordAPI - Inicio</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background: linear-gradient(90deg, #642B73, #C6426E);
                font-family: sans-serif;
            }
            
            #divPrincipal {
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-9" id="divPrincipal">
                    <h2 style="text-align: center;">- Diccionario API Oxford -</h2>
                    <h5 style="text-align: center;">Examen - Lobsang Barriga</h5>
                    <hr>
                    <p>Esta aplicación web permite buscar la definición principal de una palabra en español, rescatando información desde Oxford Dictionaries. Construido en Java Maven para la asignatura "Taller de Aplicaciones Empresariales" impartida por el profesor César Cruces en Instituto Profesional CIISA. <em><span style="color:#666666">- Lobsang Barriga, Mayo 2021</span></em></p>
                    <hr>
                    <h6><strong>Endpoints API Rest</strong></h6><br>
                    <p><strong>GET</strong> https://lobsangbarrigaexamen.herokuapp.com/api/definiciones</p>
                    <p style="font-size:90%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Permite retornar las definiciones ya buscadas en la tabla historial tbl_historial_definiciones.</p>
                    <br>
                    <p><strong>POST</strong> https://lobsangbarrigaexamen.herokuapp.com/api/definiciones/{palabra}</p>
                    <p style="font-size:90%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Permite buscar una palabra mediante la API REST que consume la API de Oxford.</p>
                    <hr>
                    <form name="frm_consulta" action="miControlador" method="POST">
                        <div class="form-group row">
                            <label for="txt_palabra" class="col-sm-3 col-form-label">Ingrese una palabra:</label>
                            <div class="col-sm-9">
                                <input type="text" name="txt_palabra" placeholder="Ej: Perro" class="form-control">
                                <p style="font-size:80%;color:#666666">* No utilizar tildes</p>
                            </div>
                        </div>
                        <input type="submit" name="btn_consultar" value="Consultar definición" class="btn btn-primary btn-block">
                        <input type="submit" name="btn_historial" value="Ver historial de búsquedas" class="btn btn-success btn-block">
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
