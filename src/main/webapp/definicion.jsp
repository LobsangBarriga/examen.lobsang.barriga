<%-- 
    Document   : definicion
    Created on : 05-05-2021, 3:57:58
    Author     : Karukami
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.entity.TblHistorialDefiniciones"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String definicion = request.getAttribute("definicion").toString();
    //List<TblHistorialDefiniciones> tblHistorialDefiniciones = (List<TblHistorialDefiniciones>) request.getAttribute("tblHistorialDefiniciones");
    //Iterator<TblHistorialDefiniciones> iteradorDefiniciones = tblHistorialDefiniciones.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OxfordAPI - Definición</title>
        <!-- BOOTSTRAP CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background: linear-gradient(90deg, #642B73, #C6426E);
                font-family: sans-serif;
            }
            
            #divPrincipal {
                background-color: white;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-9" id="divPrincipal">
                    <h2 style="text-align: center;">- Diccionario API Oxford -</h2>
                    <h5 style="text-align: center;">Examen - Lobsang Barriga</h5>
                    <hr>
                    <h6><strong>Definición encontrada:</strong></h6>
                    <hr>
                    <p><%= definicion%></p>
                    <hr>
                    <input type="button" value="Volver" class="btn btn-info btn-block" onclick="window.history.back();">
                </div>
                <div class="col"></div>
            </div>
        </div>
        <p></p>
    </body>
</html>
