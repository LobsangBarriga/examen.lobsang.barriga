/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.entity.TblHistorialDefiniciones;

/**
 *
 * @author Karukami
 */
public class TblHistorialDefinicionesJpaController implements Serializable {

    // ATRIBUTOS
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit"); 

    public TblHistorialDefinicionesJpaController() {
        
    }
    
    public TblHistorialDefinicionesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    //private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblHistorialDefiniciones tblHistorialDefiniciones) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tblHistorialDefiniciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTblHistorialDefiniciones(tblHistorialDefiniciones.getId()) != null) {
                throw new PreexistingEntityException("TblHistorialDefiniciones " + tblHistorialDefiniciones + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblHistorialDefiniciones tblHistorialDefiniciones) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tblHistorialDefiniciones = em.merge(tblHistorialDefiniciones);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblHistorialDefiniciones.getId();
                if (findTblHistorialDefiniciones(id) == null) {
                    throw new NonexistentEntityException("The tblHistorialDefiniciones with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblHistorialDefiniciones tblHistorialDefiniciones;
            try {
                tblHistorialDefiniciones = em.getReference(TblHistorialDefiniciones.class, id);
                tblHistorialDefiniciones.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblHistorialDefiniciones with id " + id + " no longer exists.", enfe);
            }
            em.remove(tblHistorialDefiniciones);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblHistorialDefiniciones> findTblHistorialDefinicionesEntities() {
        return findTblHistorialDefinicionesEntities(true, -1, -1);
    }

    public List<TblHistorialDefiniciones> findTblHistorialDefinicionesEntities(int maxResults, int firstResult) {
        return findTblHistorialDefinicionesEntities(false, maxResults, firstResult);
    }

    private List<TblHistorialDefiniciones> findTblHistorialDefinicionesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblHistorialDefiniciones.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblHistorialDefiniciones findTblHistorialDefiniciones(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblHistorialDefiniciones.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblHistorialDefinicionesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblHistorialDefiniciones> rt = cq.from(TblHistorialDefiniciones.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
