/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Math.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.entity.TblHistorialDefiniciones;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import root.dto.PalabraDTO;
import org.json.*;
import root.dao.TblHistorialDefinicionesJpaController;

/**
 *
 * @author Karukami
 */
@WebServlet(name = "miControlador", urlPatterns = {"/miControlador"})
public class miControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet miControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet miControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // BOTON BUSCAR DEFINICION
        if(request.getParameter("btn_consultar") != null){
            // Recepción de data
            String palabra = request.getParameter("txt_palabra");

            // Declara un nuevo cliente
            Client client = ClientBuilder.newClient();

            // Declara un nuevo recurso con la api de oxford
            WebTarget miRecurso = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabra);

            // Retorname algo porfa
            PalabraDTO retorno = miRecurso.request(MediaType.APPLICATION_JSON).header("app_id","8a6077d1").header("app_key","d9fc7dc3f7c002029015fbc4ac419b8a").get(PalabraDTO.class);

            // Debug
            System.out.println("El retorno es: " + retorno.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0));

            // Asigna valor a definicion
            // Es la cuestion mas trucha ever, pero funcionaaaaa por fin funcionaaaa
            String definicion = retorno.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0);

            // Creamos la entidad
            TblHistorialDefiniciones historialEntity = new TblHistorialDefiniciones();

            // Saca fecha y hora
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            System.out.println("Super fecha y hora: " + formatter.format(date));
            Date fechahora = date;

            // Datos para id random
            int min = 1;
            int max = 2147483645;

            // Define atributos del entity
            historialEntity.setId((int)Math.floor(Math.random()*(max-min+1)+min));
            historialEntity.setPalabra(palabra);
            historialEntity.setDefinicion(definicion);
            historialEntity.setFechaHora(fechahora);

            // Debug para ver que le estoy pasando a la tabla
            System.out.println("setId: " + historialEntity.getId());
            System.out.println("setPalabra: " + historialEntity.getPalabra());
            System.out.println("setDefinicion: " + historialEntity.getDefinicion());
            System.out.println("setFechaHora: " + historialEntity.getFechaHora());

            // Instancia dao
            TblHistorialDefinicionesJpaController dao = new TblHistorialDefinicionesJpaController();

            try {
                dao.create(historialEntity);
            } catch (Exception ex) {
                Logger.getLogger(miControlador.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Pasa la lista al request
            request.setAttribute("definicion", definicion);

            // Redirige a pagina de resultado
            request.getRequestDispatcher("definicion.jsp").forward(request, response);

            //processRequest(request, response);
        } else if (request.getParameter("btn_historial") != null){
            List <TblHistorialDefiniciones> listaDefiniciones;
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            EntityManager em = emf.createEntityManager();
            try{
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(TblHistorialDefiniciones.class));
                Query q = em.createQuery(cq);

                listaDefiniciones = q.getResultList();
                
                mostrarLista(request, response, listaDefiniciones);
            } catch (Exception ex) {
                System.out.println(ex.toString());
            } 
            finally {
                em.close();
            }
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void mostrarLista(HttpServletRequest request, HttpServletResponse response, List<TblHistorialDefiniciones> listaDefiniciones)
        throws ServletException, IOException{
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>OxfordAPI - Base de datos</title>");
            out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
                        "        <style>\n" +
                        "            body {\n" +
                        "                background: linear-gradient(90deg, #642B73, #C6426E);\n" +
                        "                font-family: sans-serif;\n" +
                        "            }\n" +
                        "            \n" +
                        "            #divPrincipal {\n" +
                        "                background-color: white;\n" +
                        "                margin-top: 30px;\n" +
                        "                padding: 30px;\n" +
                        "                border-radius: 10px;\n" +
                        "            }\n" +
                        "        </style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div class=\"container-fluid\">\n" +
                        "            <div class=\"row\">\n" +
                        "                <div class=\"col\"></div>\n" +
                        "                <div class=\"col-10\" id=\"divPrincipal\">");
            out.println("<h4>Historial de búsquedas</h4>");
            out.println("<hr>");
            out.println("<table class='table table-bordered table-sm table-hover'>");
            out.println("<tr>");
            out.println("<thead>");
            out.println("<th>ID</th>");
            out.println("<th>PALABRA</th>");
            out.println("<th>DEFINICION</th>");
            out.println("<th>FECHA Y HORA</th>");
            out.println("</thead>");
            out.println("</tr>");
            for(int i = 0; i < listaDefiniciones.size(); i++){
                out.println("<tr>");
                out.println("<td>" + listaDefiniciones.get(i).getId() + "</td>");
                out.println("<td>" + listaDefiniciones.get(i).getPalabra() + "</td>");
                out.println("<td>" + listaDefiniciones.get(i).getDefinicion() + "</td>");
                out.println("<td>" + listaDefiniciones.get(i).getFechaHora()+ "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("<input type=\"button\" value=\"Volver\" class=\"btn btn-info btn-block\" onclick=\"window.history.back();\">");
            out.println("</div>\n" +
                        "                <div class=\"col\"></div>\n" +
                        "            </div>\n" +
                        "        </div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
}
